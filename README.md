# Love Letter Console Game

## Description
This Java console application is an implementation of the popular card game "Love Letter." "Love Letter" is a strategic card game for 2 to 4 players.

## Gameplay
The goal of "Love Letter" is to win the affection of the princess by delivering her love letters. Players take turns drawing and playing cards with unique abilities to eliminate other players or protect themselves. The player with the highest-ranking card at the end of the round wins the princess's love.

## Rules
The game enforces the rules of "Love Letter," including card rankings, actions, and win conditions. You can refer to the in-game instructions for a summary of the rules.
The original rules can be found here: https://alderac.com/wp-content/uploads/2017/11/Love-Letter-Premium_Rulebook.pdf

## Contributors
Isabell Hans - https://gitlab2.cip.ifi.lmu.de/hansi

## Notes
This project was developed as part of the Softwareentwicklungspraktikum in Wintersemester 2023/24


Enjoy playing "Love Letter" and may the best player win the princess's heart!
