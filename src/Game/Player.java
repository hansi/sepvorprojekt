package game;

import java.util.ArrayList;

import game.cards.Card;

/**
 * This class provides functionalities and properties regarding the players
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Player {
    private String name;
    private int score;
    private boolean isActive;
    private boolean isProtected;
    private ArrayList<Card> handcards;
    private ArrayList<Card> discardedCards;

    /**
     * Constructor for the class Player
     * 
     * @param name The requested name for this player
     */
    public Player(String name) {
        setName(name);
        score = 0;
        setActiveStatus(true);
        setProtection(false);
        handcards = new ArrayList<>();
        discardedCards = new ArrayList<>();
    }

    
    /**
     * Sets the name of this player
     * 
     * @param name The name of this player
     */
    private void setName(String name) {
        this.name = name;
    }
    
    /**
     * Returns the name of this player
     * 
     * @return The name of this player
     */
    public String getName() {
        return this.name;
    }

    /**
     * Increases the score of this player
     */
    public void increaseScore() {
        this.score++;
    }

    /**
     * Returns the score of this player
     * 
     * @return The score of this player
     */
    public int getScore() {
        return this.score;
    }

    /**
     * Returns if this player is an active player of the round
     * 
     * @return The active status
     */
    public boolean isActive() {
        return this.isActive;
    }

    /**
     * This method checks if this player is currently protected
     * 
     * @return If this player is protected
     */
    public boolean isProtected() {
        return this.isProtected;
    }

    /**
     * This method is used to draw a card from the deck
     * 
     * @param deck The deck to draw the card from
     * @return The drawn card
     */
    public Card drawCard(Deck deck) {
        Card card = deck.draw();
        this.handcards.add(card);
        setCardList(handcards);
        return card;
    }

    /**
     * This method is used to draw a specific card
     * 
     * @param card The card to be drawn
     * @return The drawn card
     */
    public Card drawCard(Card card) {
        this.handcards.add(card);
        return card;
    }

    /**
     * This method replaces this players list of handcards with a new one
     * 
     * @param cards The new list of cards
     */
    public void setCardList(ArrayList<Card> cards) {
        this.handcards = cards;
    }

    /**
     * This method returns the list of this players handcards
     * 
     * @return The ArrayList of this players handcards
     */
    public ArrayList<Card> getCardList() {
        return this.handcards;
    }

    /**
     * Returns the cards in this players hands as an arraylist
     * 
     * @return The string list of this players handcards names
     */
    public ArrayList<String> getCardNames() {
        ArrayList<String> cardList = new ArrayList<>();
        for (Card card : this.handcards) {
            cardList.add(card.getName());
        }
        return cardList;
    }

    /**
     * This method shows the card in this players hand
     * 
     * @return A string that tells about this players hand
     */
    public String showCards() {
        String cards = this.name + "'s hand: ";
        for (Card card : handcards) {
            if (handcards.indexOf(card) < handcards.size() - 1) {
                cards = cards + card.getName() + " and ";
            } else
                cards = cards + card.getName();
        }
        return cards;
    }

    /**
     * This method returns a list of this players handcard value
     * 
     * @return This players card value
     */
    public int getCardValue() {
        int value = 0;
        for (Card card : this.handcards) {
            value = card.getValue();
        }
        return value;
    }

    /**
     * Gets the sum of all values of the discarded cards of this player
     * 
     * @return The value of all discarded cards
     */
    public int getDiscardedCardsValues() {
        int sum = 0;
        for (Card card : this.discardedCards) {
            sum += card.getValue();
        }
        return sum;
    }

    /**
     * This method discards one card from this players handcards and adds it to the
     * list of discarded cards
     * 
     * @param card The card to be discarded
     */
    public void discard(Card card) {
        this.discardedCards.add(card);
        this.handcards.remove(card);
    }

    /**
     * This method discards all of the cards of this player and adds them to the
     * list of discarded cards
     */
    public void discardAll() {
        for (Card card : handcards) {
            this.discardedCards.add(card);
        }
        this.handcards.clear();
    }

    /**
     * Resets this players variables and handcards
     */
    public void reset() {
        this.clearHandcards();
        this.setActiveStatus(true);
        this.setProtection(false);
    }

    /**
     * Clears this players list of handcards
     */
    private void clearHandcards() {
        this.handcards.clear();
    }

    /**
     * Sets the active status of this player
     * 
     * @param status The active status
     */
    public void setActiveStatus(boolean status) {
        this.isActive = status;
    }

    /**
     * This method sets the protection
     * 
     * @param protection The desired protection status (true or false)
     */
    public void setProtection(boolean protection) {
        this.isProtected = protection;
    }
}
