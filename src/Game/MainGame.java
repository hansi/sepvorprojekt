package game;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is the main class for the game Love Letter
 * 
 * @author Isabell Hans
 * @version 1.0
 */

public class MainGame {
    private static final int MIN_PLAYERS = 2;
    private static final int MAX_PLAYERS = 4;
    private ArrayList<Player> players;
    private GameLogic game;
    private static Output output = new Output();

    /**
     * This is the constructor for the Main Game
     */
    public MainGame() {
        players = new ArrayList<>();
    }

    /**
     * The main method
     * 
     * @param args For optional input
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MainGame mainGame = new MainGame();
        mainGame.initializeGame(scanner);
        mainGame.begin(scanner);
        askForNewGame(scanner);
    }

    /**
     * Queries the parameters of this game on the console
     * 
     * @param scanner The scanner used in the calling method
     */
    public void initializeGame(Scanner scanner) {
        output.welcome();
        int numPlayers = getNumberOfPlayers(scanner);
        createPlayers(scanner, numPlayers);
    }

    /**
     * Reads requested number of players from the console and
     * checks if number is valid
     * 
     * @param scanner The scanner used in the calling method
     * @return The number of players entered in the console as Integer
     */
    private int getNumberOfPlayers(Scanner scanner) {
        int numPlayers = 0;
        boolean validInput = false;
        while (!validInput) {
            output.askNumPlayers();
            if (scanner.hasNextInt()) {
                numPlayers = scanner.nextInt();
                if (numPlayers >= MIN_PLAYERS && numPlayers <= MAX_PLAYERS) {
                    validInput = true;
                    break;
                } else {
                    output.noValidNumber();
                }
            } else {
                output.noNumber();
                scanner.next();
            }
        }
        return numPlayers;
    }

    /**
     * Creates requested number of players and
     * stores each with a valid entered name
     * 
     * @param scanner    The scanner used in the calling method
     * @param numPlayers The requested number of players
     */
    private void createPlayers(Scanner scanner, int numPlayers) {
        String playerName = "";
        scanner.nextLine();
        outerloop: for (int i = 1; i <= numPlayers; i++) {
            System.out.println("Insert name of player " + i + ": ");
            playerName = scanner.nextLine();
            while (playerName.equals("")) {
                output.insertName();
                i--;
                continue outerloop;
            }
            for (Player existingPlayer : players) {
                if (existingPlayer.getName().equals(playerName)) {
                    output.nameExists();
                    i--;
                    continue outerloop;
                }
            }
            players.add(new Player(playerName));
        }
    }

    /**
     * This method is used to begin the main game
     * 
     * @param scanner Scanner of the calling method
     */
    private void begin(Scanner scanner) {
        output.startHelp();
        handleCommand(scanner, scanner.nextLine());
    }

    /**
     * Asks for a new game
     * 
     * @param scanner The scanner used in the calling method
     */
    private static void askForNewGame(Scanner scanner) {
        output.confirm();
        String answer = scanner.nextLine();
        if (answer.equals("yes")) {
            main(null);
        }
    }

    /**
     * This method handles the input at the beginning of the main game
     * 
     * @param scanner Scanner of the calling method
     * @param command Input taken by the scanner
     */
    private void handleCommand(Scanner scanner, String command) {
        switch (command) {
            case "\\start":
                startNewGame(scanner, players);
                break;
            case "\\help":
                output.helpBegin();
                handleCommand(scanner, scanner.nextLine());
                break;
            default:
                output.noValidCommand();
                handleCommand(scanner, scanner.nextLine());
                break;
        }
    }

    /**
     * This method starts a new game round
     * 
     * @param scanner The scanner used in the calling method
     * @param players Players to play with
     */
    public void startNewGame(Scanner scanner, ArrayList<Player> players) {
        this.game = new GameLogic(players);
        game.start(scanner);
    }

}
