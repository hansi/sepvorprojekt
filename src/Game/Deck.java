package game;

import java.util.ArrayList;
import java.util.Collections;

import game.cards.Baron;
import game.cards.Card;
import game.cards.Countess;
import game.cards.Guard;
import game.cards.Handmaid;
import game.cards.King;
import game.cards.Priest;
import game.cards.Prince;
import game.cards.Princess;

/**
 * This class provides the functionality of a deck of cards
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Deck {
    private ArrayList<Card> cards;

    /**
     * Constructor for the class Deck
     */
    public Deck() {
        cards = new ArrayList<>();
        initialize(cards);
    }


    /**
     * This method initalizes this deck with the right number of the different cards
     * 
     * @param cards The ArrayList to store the diffrent cards in
     */
    private void initialize(ArrayList<Card> cards) {
        cards.add(new Princess());
        cards.add(new Countess());
        cards.add(new King());
        cards.add(new Prince());
        cards.add(new Prince());
        cards.add(new Handmaid());
        cards.add(new Handmaid());
        cards.add(new Baron());
        cards.add(new Baron());
        cards.add(new Priest());
        cards.add(new Priest());
        for (int i = 0; i < 5; i++) {
            cards.add(new Guard());
        }
    }
    
    /**
     * This method randomly shuffles the deck
     */
    public void shuffle() {
        Collections.shuffle(this.cards);
    }

    /**
     * This method draws and returns the first card
     * 
     * @return Returns card, in case of empty deck returns null
     */
    public Card draw() {
        if (!this.cards.isEmpty()) {
            Card card = this.cards.get(0);
            this.cards.remove(0);
            return card;
        } else
            return null;
    }

    /**
     * Checks if this deck is empty
     * 
     * @return If this deck is empty
     */
    public boolean isEmpty() {
        return this.cards.isEmpty();
    } 
}
