package game;

/**
 * This class holds all the prints of the other classes
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Output {
    /**
     * Constructor for the class Output
     */
    public Output() {
    }

    /**
     * Used to welcome players to the game
     */
    public void welcome() {
        System.out.println(
                "Welcome to Love Letter! Can you win Princess Annettes heart? This game is for 2 to 4 players.");
    }

    /**
     * Used to start round 1
     */
    public void startFirstRound() {
        System.out.println();
        System.out.println(
                "Let's start the first round! Try to play your cards wisely so your letter reaches the Princess!");
        System.out.println();
    }

    /**
     * Used to start a new round
     */
    public void newRound() {
        System.out.println("It's a new day! Let's start a new round!");
    System.out.println();
    }

    /**
     * Used to request number of players
     */
    public void askNumPlayers() {
        System.out.println("How many players do you need?");
    }

    /**
     * Used to request the names of the players
     */
    public void insertName() {
        System.out.println("Please insert a name!");
    }

    /**
     * Used to figure out who starts the round
     */
    public void dateQuestion() {
        System.out.println();
        System.out.println(
                "Whoever was most recently on a date goes first. If tied, the youngest player starts. Please enter the name of the first player: ");
    }

    /**
     * Used ask if player wants to start a new game
     */
    public void newGame() {
        System.out.println();
        System.out.println(
                "Do you really want to start a new game? Your current scores wont be saved!");
    }

    /**
     * Used to confirm a question asked before
     */
    public void confirm() {
        System.out.println("Type in \"yes\" if you want to start a new game, or anything else to end this program: ");
    }

    /**
     * Informs about cancellation of action
     */
    public void cancelled() {
        System.out.println("Cancelled. Please type in a command: ");
    }

    /**
     * Used to print out options at the beginning of a new game
     */
    public void startHelp() {
        System.out.println("Type in \\start to start the game. You can always type in \\help for help!");
    }

    /**
     * Used to tell player what the options are
     */
    public void actionOrHelp() {
        System.out.println("Perform an action or try \\help for help: ");
    }

    /**
     * Used to tell player to type in command
     */
    public void newCommand() {
        System.out.println();
        System.out.println("Type in a new command: ");
    }

    /**
     * Prints out possible commands at the beginning of a game
     */
    public void helpBegin() {
        System.out.println("Try these commands: ");
        System.out.println("\\help to show help");
        System.out.println("\\start to start the game");
    }

    /**
     * Prints out possible commands during a game
     */
    public void showCommandExplanations() {
        System.out.println();
        System.out.println("Try these commands: ");
        System.out.println("\\help to show help");
        System.out.println("\\start to start a new game");
        System.out.println("\\playCard to play a card");
        System.out.println("\\showHand to show your cards");
        System.out.println("\\showScore to show current score");
        System.out.println("\\showPlayers to show all players and their status");
    }

    /**
     * Explains functionality of card Baron
     */
    public void baronText() {
        System.out.println(
                "The Baron lets you choose another player in the round and compare your hands. The player with a lower number gets knocked out of the round. In case of a tie, nothing happens.");
    }

    /**
     * Explains functionality of card Countess
     */
    public void countessText() {
        System.out.println(
                "The Countesses effect only applies while it's in your hand. You can discard it at any time, but you have to discard it if you have either the King or Prince in your hand. You do not have to reveal the other card. ");
    }

    /**
     * Explains functionality of card Gaurd
     */
    public void guardText() {
        System.out.println(
                "The Guard lets you choose another player and name a number between 2 and 8. If that player has that number in their hand, he or she gets knocked out of the round. If the other player holds the Handmaid, this card is discarded without effect.");
    }

    /**
     * Explains functionality of card Handmaid
     */
    public void handmaidText() {
        System.out.println(
                "When discarding the Handmaid, you are immune to the effects of other cards until the start of the next round.");
    }

    /**
     * Explains functionality of card King
     */
    public void kingText() {
        System.out.println(
                "The discarding ot the King allows you to trade the card in your hand with the one of another player who is still in the round.");
    }

    /**
     * Explains functionality of card Priest
     */
    public void priestText() {
        System.out.println("When you discard the Priest, you can look at another player's hand.");
    }

    /**
     * Explains functionality of card Prince
     */
    public void princeText() {
        System.out.println(
                "When you discard the Prince, choose one player still in the round (including yourself). That player discards his or her hand (without applying its effect, unless it is the Princess) and draws a new one. If the deck is empty and the player cannot draw a card, that player draws the card that was removed at the start of the round. If all other players are protected by the Handmaid, you must choose yourself.");
    }

    /**
     * Explains functionality of card Princess
     */
    public void princessText() {
        System.out.println(
                "If you discard the Princess, she has tossed your letter into the fire. You are immediately knocked out of the round.");
    }

    /**
     * Used if there is a tie
     */
    public void tie() {
        System.out.println("Tie! None of you get's knocked out of the round.");
    }

    /**
     * Used to tell that a player got knocked out of the round
     */
    public void knockedOut() {
        System.out.print(" got knocked out of the round.");
        System.out.println();
    }

    /**
     * Used to tell that a player can stay in the round
     */
    public void lucky() {
        System.out.print(" was lucky and can stay in this round.");
        System.out.println();
    }

    /**
     * Used to tell that player is now protected
     */
    public void proected() {
        System.out.println("You are protected for this round!");
    }

    /**
     * Used to tell that cards are not availabe
     */
    public void cardsNotAvailable() {
        System.out.println("These cards are not available during this round: ");
    }

    /**
     * used to tell that the first cards were drawn
     */
    public void cardsDrawn() {
        System.out.println("First cards drawn: ");
    }

    /**
     * Asks player which card he or she wants to pick
     */
    public void selectCard() {
        System.out.println("Which of these cards do you want to play?");
    }

    /**
     * Asks player which other player to pick
     */
    public void choose() {
        System.out.println("Who do you choose?");
    }

    /**
     * Asks player to choose a number
     */
    public void chooseNumber() {
        System.out.println("Name a number between 2 and 8: ");
    }

    /**
     * Used to inform player that he or she selected him- or herself
     */
    public void selectedSelf() {
        System.out.println("You selected yourself");
    }

    /**
     * Informs player that countess has to be discarded
     */
    public void discardCountess() {
        System.out.println("You have to discard Countess Wilhelmina.");
    }

    /**
     * Informs player that he or she discarded the countess
     */
    public void discardedCountess() {
        System.out.println("You discarded the Countess.");
    }

    /**
     * Used when more than one player won this round
     */
    public void moreLetters() {
        System.out.print("Princess Annette got more than one letter! The winners of this round are ");
        System.out.println();
    }

    /**
     * Used when one player one this round
     */
    public void oneLetter() {
        System.out.print("Princess Annette got a letter! The winner of this round is ");
        System.out.println();
    }

    /**
     * Used to tell who won the game
     */
    public void wonHeart() {
        System.out.print(" won Princess Annettes heart!");
        System.out.println();
    }

    /**
     * Used to tell that name already exists
     */
    public void nameExists() {
        System.out.println("Name already exists. Please insert another name!");
    }

    /**
     * Used to tell that name couln't be found
     */
    public void nameNotFound() {
        System.out.println("Name not found. Try again: ");
    }

    /**
     * Used to tell that input is no valid number
     */
    public void noValidNumber() {
        System.out.println("No valid number. Please try again: ");
    }

    /**
     * Used to tell that input is not a number
     */
    public void noNumber() {
        System.out.println("Not a number. Please try again. ");
    }

    /**
     * Used to tell that input was no command
     */
    public void noValidCommand() {
        System.out.println("No valid command. Please type in \\start or \\help to see all commands: ");
    }

    /**
     * Used to tell that input was no card name
     */
    public void noValidCardName() {
        System.out.println("No valid card name. Type in \\playCard to try again: ");
    }

    /**
     * Used to inform that no players can be chosen
     */
    public void noPlayers() {
        System.out.println("There are no players to choose from. Your card is discarded without effect.");
    }

    /**
     * Used to tell that a player can't be chosen
     */
    public void notANameOption() {
        System.out.println("You can't choose this one. Try again: ");
    }

    /**
     * Used to tell that player can't choose him- or herself
     */
    public void cantChooseSelf() {
        System.out.println("You can't choose yourself. Try again: ");
    }

    /**
     * Prints out THE END
     */
    public void end() {
        System.out.println();
        System.out.println("THE END <3");
        System.out.println();
    }
}
