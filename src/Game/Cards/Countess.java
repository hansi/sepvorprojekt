package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides special functionalities of the card "Countess Wilhelmina"
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Countess extends Card {
    private static final String NAME = "Countess Wilhelmina";
    private static final int VALUE = 7;
    private static Output output = new Output();

    /**
     * The constructor for the class Countess
     */
    public Countess() {
        super(NAME, VALUE);
    }

    /**
     * Prints out the function of this card
     */
    @Override
    public void showFunction() {
       output.countessText(); }

    /**
     * This method provides the function of this card
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    @Override
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCar) {
        output.discardedCountess();
    }
}
