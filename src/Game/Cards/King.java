package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides special functionalities of the card "King Arnaud IV"
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class King extends Card {
    private static final String NAME = "King Arnaud IV";
    private static final int VALUE = 6;
     private static Output output = new Output();

    /**
     * The constructor for the class King
     */
    public King() {
        super(NAME, VALUE);
    }

    /**
     * Prints out the function of this card
     */
    @Override
    public void showFunction() {
    output.kingText();    
    }

    /**
     * This method provides the function of this card
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    @Override
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCard) {
        Player otherPlayer = super.choosePlayer(scanner, players, turn, false);
        if (!(otherPlayer == null)) {
            ArrayList<Card> otherPlayersCards = otherPlayer.getCardList();
            ArrayList<Card> thisPlayersCards = turn.getCardList();
            otherPlayer.setCardList(thisPlayersCards);
            turn.setCardList(otherPlayersCards);
            System.out.println("You swapped your cards with " + otherPlayer.getName() + ". " + turn.showCards() + ". "+ otherPlayer.showCards());
        }
    }
}
