package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides special functionalities of the card "Guard Odette"
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Guard extends Card {
    private static final String NAME = "Guard Odette";
    private static final int VALUE = 1;
    private static Output output = new Output();

    /**
     * The constructor of the class Guard
     */
    public Guard() {
        super(NAME, VALUE);
    }

    /**
     * Prints out the function of this card
     */
    @Override
    public void showFunction() {
        output.guardText();
    }

    /**
     * This method provides the function of this card
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    @Override
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCard) {
        Player otherPlayer = super.choosePlayer(scanner, players, turn, false);
        if (!(otherPlayer == null)) {
            output.chooseNumber();
            int number = 0;
            boolean validInput = false;
            while (!validInput) {
                if (scanner.hasNextInt()) {
                    number = scanner.nextInt();
                    if (number >= 2 && number <= 8) {
                        validInput = true;
                        scanner.nextLine();
                        break;
                    } else {
                        output.noValidNumber();
                    }
                } else {
                    output.noNumber();
                    scanner.next();
                }
            }
            if (otherPlayer.getCardValue() == number) {
                otherPlayer.setActiveStatus(false);
                otherPlayer.discardAll();
                System.out.print(otherPlayer.getName());
                output.knockedOut();
            } else {
                System.out.print(otherPlayer.getName());
                output.lucky();
            }
        }
    }
}
