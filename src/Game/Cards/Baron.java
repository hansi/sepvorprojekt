package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides special functionalities of the card "Baron Talus"
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Baron extends Card {
    private static final String NAME = "Baron Talus";
    private static final int VALUE = 3;
    private static Output output = new Output();

    /**
     * The constructor for the class Baron
     */
    public Baron() {
        super(NAME, VALUE);
    }

    /**
     * This method prints out the function of this card
     */
    @Override
    public void showFunction() {
        output.baronText();
    }

    /**
     * This method provides the function of this card
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    @Override
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCard) {
        Player otherPlayer = super.choosePlayer(scanner, players, turn, false);
        if (!(otherPlayer == null)) {
            int num1 = otherPlayer.getCardValue();
            int num2 = turn.getCardValue();
            if (num1 < num2) {
                otherPlayer.setActiveStatus(false);
                otherPlayer.discardAll();
                ;
                System.out.print(otherPlayer.getName());
                output.knockedOut();
            } else if (num2 < num1) {
                turn.setActiveStatus(false);
                turn.discardAll();
                System.out.print(turn.getName());
                output.knockedOut();
            } else {
                output.tie();
            }
        }
    }
}
