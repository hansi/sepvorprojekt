package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides special functionalities of the card "Handmaid Susannah"
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Handmaid extends Card {
    private static final String NAME = "Handmaid Susannah";
    private static final int VALUE = 4;
    private static Output output = new Output();

    /**
     * The constructor for the class Handmaid
     */
    public Handmaid() {
        super(NAME, VALUE);
    }

    /**
     * Prints out the function of this card
     */
    @Override
    public void showFunction() {
        output.handmaidText();
    }

    /**
     * This method provides the function of this card
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    @Override
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCard) {
        turn.setProtection(true);
        output.proected();
    }
}
