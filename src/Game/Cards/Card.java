package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides the main functionalities of the cards
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Card {
    private String name;
    private int value;
    private static Output output = new Output();

    /**
     * Constructor for the class Card
     * 
     * @param name  The name of this card
     * @param value The value of this card
     */
    public Card(String name, int value) {
        setName(name);
        setValue(value);
    }

    /**
     * Sets the name of this card
     * 
     * @param name The name for this card
     */
    private void setName(String name) {
        this.name = name;
    }

    /**
     * Returns the name of this card
     * 
     * @return The name of this card
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the value of this card
     * 
     * @param value The value for this card
     */
    private void setValue(int value) {
        this.value = value;
    }

    /**
     * Returns the value of this card
     * 
     * @return The value of this card
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Shows the function of this card
     * To be overridden by child classes
     */
    public void showFunction() {
    }

    /**
     * Holds the effect of this card
     * To be overridden by child classes
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players to play with
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCard) {
    }

    /**
     * This method is used to select a player
     * 
     * @param scanner The scanner used in the calling method
     * @param players The list of players to play with
     * @param turn    The player whose turn it is
     * @param selfPossible True if the player is allowed to select his/her self
     * @return The chosen player
     */
    public Player choosePlayer(Scanner scanner, ArrayList<Player> players, Player turn, boolean selfPossible) {
        if (!playerExists(players, turn)) {
            output.noPlayers();
            return null;
        }
        output.choose();
        String name = scanner.nextLine();
        boolean found = false;
        outerloop: while (!found) {
            for (Player p : players) {
                if (p.getName().equals(name) && !p.isProtected() && p.isActive()) {
                    Player player = players.get(players.indexOf(p));
                    found = true;
                    if (p.equals(turn) && selfPossible) {
                        output.selectedSelf();
                        return player;
                    } else if (p.equals(turn) && !selfPossible) {
                        found = false;
                        output.cantChooseSelf();
                        name = scanner.nextLine();
                        continue outerloop;
                    } else {
                        return player;
                    }
                }
            }
            output.notANameOption();
            name = scanner.nextLine();
        }
        return null;
    }

    /**
     * Checks if there are any players to select from
     * 
     * @param players The list of players in this game
     * @param turn    The player whose turn it is
     * @return If there is a player to select
     */
    private boolean playerExists(ArrayList<Player> players, Player turn) {
        boolean suitablePlayersExist = false;
        for (Player p : players) {
            if (!p.isProtected() && p.isActive() && !p.equals(turn)) {
                suitablePlayersExist = true;
                break;
            }
        }
        if (!suitablePlayersExist) {
            return false;
        } else {
            return true;
        }
    }
}
