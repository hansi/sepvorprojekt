package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides special functionalities of the card "Priest Tomas"
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Priest extends Card {
    private static final String NAME = "Priest Tomas";
    private static final int VALUE = 2;
     private static Output output = new Output();

    /**
     * The constructor of the class Priest
     */
    public Priest() {
        super(NAME, VALUE);
    }

    /**
     * Prints out the function this card will have
     */
    @Override
    public void showFunction() {
        output.priestText();
    }

    /**
     * This method provides the function of this card
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    @Override
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCard) {
        Player otherPlayer = super.choosePlayer(scanner, players, turn, false);
        if (!(otherPlayer == null)) {
            System.out.println(otherPlayer.showCards());
        }
    }
}
