package game.cards;

import java.util.ArrayList;
import java.util.Scanner;

import game.Deck;
import game.Output;
import game.Player;

/**
 * This class provides special functionalities of the card "Prince Arnaud"
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class Prince extends Card {
    private static final String NAME = "Prince Arnaud";
    private static final int VALUE = 5;
    private static Output output = new Output();

    /**
     * The constructor for the class Prince
     */
    public Prince() {
        super(NAME, VALUE);
    }

    /**
     * Prints out the function of this card
     */
    @Override
    public void showFunction() {
        output.princeText();
    }

    /**
     * This method provides the function of this card
     * 
     * @param scanner   The scanner used by the calling method
     * @param players   The list of players
     * @param turn      The player whose turn it is
     * @param deck      The current deck of cards
     * @param firstCard The card drawn in the beginning
     */
    @Override
    public void play(Scanner scanner, ArrayList<Player> players, Player turn, Deck deck, Card firstCard) {
        Player otherPlayer = super.choosePlayer(scanner, players, turn, true);
        if (!(otherPlayer == null)) {
            if (otherPlayer.getCardNames().contains("Princess Annette")) {
                otherPlayer.setActiveStatus(false);
                otherPlayer.discardAll();
                System.out.print(otherPlayer.getName());
                output.knockedOut();
            } else {
                otherPlayer.discardAll();
                if (!deck.isEmpty()) {
                    otherPlayer.drawCard(deck);
                } else {
                    otherPlayer.drawCard(firstCard);
                }
            }
        }
    }
}
