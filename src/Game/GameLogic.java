package game;

import java.util.ArrayList;
import java.util.Scanner;

import game.cards.Card;

/**
 * This class provides the logic for a game
 * 
 * @author Isabell Hans
 * @version 1.0
 */
public class GameLogic {
    private ArrayList<Player> players;
    private Deck deck;
    private Player turn;
    private Card firstCard;
    private static int WIN_2PLAYERS = 7;
    private static int WIN_3PLAYERS = 5;
    private static int WIN_4PLAYERS = 4;
    private static Output output = new Output();

    /**
     * Constructor for the class GameLogic
     * 
     * @param players The list of players
     */
    public GameLogic(ArrayList<Player> players) {
        this.players = players;
    }

    /**
     * This method starts a new game round by drawing cards
     * 
     * @param scanner The scanner used in the calling method
     */
    public void start(Scanner scanner) {
        output.startFirstRound();
        resetPlayers();
        deck = new Deck();
        deck.shuffle();
        firstCard = deck.draw();
        if (players.size() == 2) {
            sortOutCards();
        }
        output.cardsDrawn();
        for (Player player : players) {
            player.drawCard(deck);
            System.out.println(player.showCards());
        }
        play(scanner, selectPlayer(scanner, players));
    }

    /**
     * Starts a new Round
     * 
     * @param scanner The scanner used in the calling method
     * @param player  The player who may start this round
     */
    private void startRound(Scanner scanner, Player player) {
        output.newRound();
        resetPlayers();
        deck = new Deck();
        deck.shuffle();
        firstCard = deck.draw();
        if (players.size() == 2) {
            sortOutCards();
        }
        output.cardsDrawn();
        for (Player p : players) {
            p.drawCard(deck);
            System.out.println(p.showCards());
        }
        play(scanner, player);
    }

    /**
     * Sorts out the cards that can't be used in this round
     */
    private void sortOutCards() {
        output.cardsNotAvailable();
        for (int i = 0; i < 3; i++) {
            System.out.println(deck.draw().getName());
        }
        System.out.println();
    }

    /**
     * Resets the values of all players
     */
    private void resetPlayers() {
        for (Player player : players) {
            player.reset();
        }
    }

    /**
     * This method selects the first player
     * 
     * @param scanner The scanner used in the calling method
     * @param players The list of players to select the first one from
     * @return The selected player
     */
    private Player selectPlayer(Scanner scanner, ArrayList<Player> players) {
        output.dateQuestion();
        String name = scanner.nextLine();
        boolean found = false;
        while (!found) {
            for (Player player : players) {
                if (player.getName().equals(name)) {
                    found = true;
                    System.out.println();
                    System.out.println(name + " begins!");
                    return player;
                }
            }
            output.nameNotFound();
            name = scanner.nextLine();
        }
        return null;
    }

    /**
     * This method is used to identify the next player
     * 
     * @param scanner The scanner used in the calling method
     */
    private void selectNextPlayer(Scanner scanner) {
        int playerIndex = players.indexOf(turn);
        do {
            playerIndex = (playerIndex + 1) % players.size();
            this.turn = players.get(playerIndex);
        } while (!turn.isActive());

        if (!checkEnd()) {
            turn.setProtection(false);
            this.play(scanner, turn);
        } else {
            Player winner = getWinner(scanner);
            if (winner == null || hasFinalWinner()) {
                output.end();
            } else {
                System.out.println();
                startRound(scanner, winner);
            }
        }
    }

    /**
     * This method starts a round in the current game
     * 
     * @param scanner The scanner used in the calling method
     * @param player  The player whose turn it is
     */
    public void play(Scanner scanner, Player player) {
        this.turn = player;
        System.out.println();
        System.out.println("It's " + player.getName() + "'s turn!");
        System.out.println("The new card of " + player.getName() + " is " + player.drawCard(deck).getName());
        output.actionOrHelp();
        handleCommand(scanner, turn);
    }

    /**
     * This method handles the input from the console
     * 
     * @param scanner The scanner used in the calling method
     * @param turn    The player whose turn it is
     */
    private void handleCommand(Scanner scanner, Player turn) {
        Player player = turn;
        String command = scanner.nextLine();
        switch (command) {
            case "\\start":
                startNewGame(scanner);
                break;
            case "\\playCard":
                selectCard(scanner, player);
                break;
            case "\\showHand":
                System.out.println(player.showCards());
                output.newCommand();
                handleCommand(scanner, player);
                break;
            case "\\showScore":
                System.out.println("The score of player " + player.getName() + " is: " + player.getScore());
                output.newCommand();
                handleCommand(scanner, player);
                break;
            case "\\showPlayers":
                showPlayers();
                output.newCommand();
                handleCommand(scanner, player);
                break;
            case "\\help":
                output.showCommandExplanations();
                output.newCommand();
                handleCommand(scanner, player);
                break;
            default:
                output.noValidCommand();
                handleCommand(scanner, player);
                break;
        }
    }
    
    /**
     * Shows all of the players and their status
     */
    private void showPlayers() {
        for (Player x : players) {
            if (x.isActive()) {
                System.out.println(x.getName() + " is still active.");
            } else {
                System.out.println(x.getName() + " already got knocked out of this round.");
            }
        }
    }

        /**
     * Queries the card to play with
     * 
     * @param scanner The scanner used in the calling method
     * @param player  The player whose turn it is
     */
    private void selectCard(Scanner scanner, Player player) {
        Card card = null;
        System.out.println();
        System.out.println(player.showCards());
        System.out.println();
        for (Card c : player.getCardList()) {
            c.showFunction();
            System.out.println();
        }
        if (player.getCardNames().contains("Countess Wilhelmina")
                && (player.getCardNames().contains("Prince Arnaud")
                        || player.getCardNames().contains("King Arnaud IV"))) {
            output.discardCountess();
            card = player.getCardList().get(player.getCardNames().indexOf("Countess Wilhelmina"));
            playCard(scanner, card);
        } else {
            output.selectCard();
            String cardName = scanner.nextLine();
            if (player.getCardNames().contains(cardName)) {
                card = player.getCardList().get(player.getCardNames().indexOf(cardName));
                playCard(scanner, card);
            } else {
                output.noValidCardName();
                handleCommand(scanner, player);
            }
        }
    }

    /**
     * This method handles the playing of one card
     * 
     * @param scanner The sanner used in the calling method
     * @param card    The card to discard
     */
    private void playCard(Scanner scanner, Card card) {
        System.out.println();
        System.out.println("Card \"" + card.getName() + "\" selected");
        turn.discard(card);
        card.play(scanner, players, turn, deck, firstCard);
        selectNextPlayer(scanner);
    }
    
    /**
     * Checks if the game round is over
     * 
     * @return True if the game round is not over
     */
    private boolean checkEnd() {
        int activePlayerCount = 0;
        String endReason = "";
        for (Player player : players) {
            if (player.isActive()) {
                activePlayerCount++;
            }
        }
        if (activePlayerCount <= 1) {
            endReason = "No more players left. ";
        } else if (deck.isEmpty()) {
            endReason = "The deck is empty. ";
        }
        if (!endReason.isEmpty()) {
            System.out.println();
            System.out.println(endReason + "This game round is over!");
        }
        return !endReason.isEmpty();
    }

    /**
     * Returns the winner who will start the next round
     * 
     * @param scanner The scanner used in the calling method
     * @return The winner
     */
    private Player getWinner(Scanner scanner) {
        ArrayList<Player> playersWithHighestValue = new ArrayList<>();
        int highestValue = -1;
        for (Player player : players) {
            if (player.isActive()) {
                int playerCardValue = player.getCardValue();
                if (playerCardValue > highestValue) {
                    highestValue = playerCardValue;
                    playersWithHighestValue.clear();
                    playersWithHighestValue.add(player);
                } else if (playerCardValue == highestValue) {
                    playersWithHighestValue.add(player);
                }
            }
        }
        if (playersWithHighestValue.size() > 1) {
            int highestDiscardedValue = -1;
            for (Player player : playersWithHighestValue) {
                int discardedValue = player.getDiscardedCardsValues();
                if (discardedValue > highestDiscardedValue) {
                    highestDiscardedValue = discardedValue;
                }
            }
            ArrayList<Player> winners = new ArrayList<>();
            for (Player player : playersWithHighestValue) {
                int discardedValue = player.getDiscardedCardsValues();
                if (discardedValue == highestDiscardedValue) {
                    winners.add(player);
                }
            }
            return printWinners(scanner, winners);
        } else {
            return printWinner(scanner, playersWithHighestValue);
        }
    }

    /**
     * Prints out and returns the winner
     * 
     * @param scanner The scanner used in the calling method
     * @param winners The list of winners
     * @return The winner
     */
    private Player printWinner(Scanner scanner, ArrayList<Player> winners) {
        winners.get(0).increaseScore();
        output.oneLetter();
        System.out.print(winners.get(0).getName());
        printScores();
        return winners.get(0);
    }

    /**
     * Selects who won if players have the same card values
     * 
     * @param scanner The scanner used in the calling method
     * @param winners The list of winners to select from
     * @return The winner
     */
    private Player printWinners(Scanner scanner, ArrayList<Player> winners) {
        if (winners.size() > 1) {
            output.moreLetters();
            for (int i = 0; i < winners.size(); i++) {
                winners.get(i).increaseScore();
                System.out.print(winners.get(i).getName());
                if (i < winners.size() - 1) {
                    System.out.print(" and ");
                }
            }
            printScores();
            if (!hasFinalWinner()) {
                return selectPlayer(scanner, winners);
            } else
                return null;
        } else {
            winners.get(0).increaseScore();
            output.oneLetter();
            System.out.print(winners.get(0).getName());
            printScores();
            return winners.get(0);
        }
    }

        /**
     * Prints out the scores of all players
     */
    private void printScores() {
        System.out.println();
        for (Player player : players) {
            System.out.println("The score of " + player.getName() + " is " + player.getScore());
        }
        System.out.println();
    }

    /**
     * Checks if the game has a final winner
     * 
     * @return If the game has a final winner
     */
    private boolean hasFinalWinner() {
        boolean hasFinalWinner = false;
        for (Player player : players) {
            if (player.getScore() == WIN_2PLAYERS && players.size() == 2) {
                printFinalWinner(player);
                hasFinalWinner = true;
            } else if (player.getScore() == WIN_3PLAYERS && players.size() == 3) {
                printFinalWinner(player);
                hasFinalWinner = true;
            } else if (player.getScore() == WIN_4PLAYERS && players.size() == 4) {
                printFinalWinner(player);
                hasFinalWinner = true;
            }
        }
        return hasFinalWinner;
    }

    /**
     * Prints out who won the game
     * 
     * @param player The player who won the game
     */
    private void printFinalWinner(Player player) {
        System.out.print(player.getName());
        output.wonHeart();
    }

        /**
     * This method starts a new game
     * 
     * @param scanner The scanner used in the calling method
     */
    public void startNewGame(Scanner scanner) {
        output.newGame();
        output.confirm();
        switch (scanner.nextLine()) {
            case "yes":
                System.out.println();
                MainGame newGame = new MainGame();
                newGame.initializeGame(scanner);
                break;
            default:
                output.cancelled();
                handleCommand(scanner, turn);
                break;
        }
    }

}
